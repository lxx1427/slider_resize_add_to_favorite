var images = ['https://tovaroved-expert.ru/wp-content/uploads/2019/02/3_d-350x200.jpg', 
'http://www.bayvista.com.au/wp-content/uploads/2016/08/home-ice-cream@2x-350x200.jpg', 
'https://soleks.com.ua/wp-content/uploads/2017/01/cuba-350x200.jpg', 
'http://newhorizons.fi/wp-content/plugins/special-recent-posts-pro/cache/srpthumb-p3862-350x200-no.jpg']



function Slider(containerSlider, arrOfimages, containerSelected) {
    this.container = document.getElementsByClassName(containerSlider)[0];
    this.image = document.createElement('img');
    this.index = 0;
    this.image.src = arrOfimages[this.index];
    this.wrapper = document.createElement('div');
    this.leftButton = document.createElement('i');
    this.rightButton = document.createElement('i');
    this.resizeButton = document.createElement('i');
    this.selectButton = document.createElement('i');
    this.leftButton.className = 'fas fa-arrow-alt-circle-left';
    this.rightButton.className = 'fas fa-arrow-alt-circle-right active';
    this.resizeButton.className = 'fas fa-search-plus';
    this.selectButton.className = 'fas fa-sign-in-alt';
    this.containerSelected = document.getElementsByClassName(containerSelected)[0];
    this.arrSelectedImages = [];

    this.render = function() {
        this.container.appendChild(this.wrapper);
        this.wrapper.appendChild(this.image);
        this.wrapper.appendChild(this.leftButton);
        this.wrapper.appendChild(this.rightButton);
        this.wrapper.appendChild(this.resizeButton);
        this.wrapper.appendChild(this.selectButton);
    }
    this.render();

    this.left = function() {
        if(this.index>0) {
            this.image.remove();
            this.index--;
            this.image.src = arrOfimages[this.index];
            this.wrapper.appendChild(this.image);
            this.rightButton.classList.add('active');
        }
        if(this.index==0) {
            this.leftButton.classList.remove('active');
        }
    }.bind(this);
    this.right = function() {
        if(this.index<arrOfimages.length-1) {
            this.image.remove();
            this.index++;
            this.image.src = arrOfimages[this.index];
            this.wrapper.appendChild(this.image);
            this.leftButton.classList.add('active');
        }
        if(this.index==arrOfimages.length-1) {
            this.rightButton.classList.remove('active');
        }
    }.bind(this);

    this.resize = function() {
        this.wrapper.classList.toggle('resize');
    }.bind(this);

    this.leftButton.onclick = this.left;
    this.rightButton.onclick = this.right;
    this.resizeButton.onclick = this.resize;

    this.select = function() {
        if (!~this.arrSelectedImages.indexOf(this.image.src)) {
        this.selectImage = document.createElement('img');
        this.deleteButton = document.createElement('i');
        this.selectImage.src = this.image.src;
        this.containerSelected.appendChild(this.selectImage);
        this.arrSelectedImages.push(this.image.src);
        } else {
            alert(`You have already add this image!`);
        }
    }.bind(this);

    this.removeSelected = function(event) {
        if(event.target.src) {
            var question = confirm('Do you wanna to delete this image from selected?');
            if(question) {
                this.arrSelectedImages.splice(this.arrSelectedImages.indexOf(event.target.src), 1);
                event.target.remove();
            }
        }
    }.bind(this);

    this.selectButton.onclick = this.select;
    this.containerSelected.onclick = this.removeSelected;
}

var slider = new Slider('slider', images, 'selected');

